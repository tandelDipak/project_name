# Project Template Description

#### Folder : project_name
- This is a project folder. 
- Below folders and files should be inside this folder.

#### Folder : scripts
- Put all your notebook and python files in this folder.

#### Folder : keys
- Put all the keys, needed to login to cloud provider here.

#### Folder : models
- Put all the model artifacts in this folder.

#### File : config.py
- Define system dependent variables and other variables here.
- For example : Path of the project.

#### File : requirements.txt
- List of the project dependencies goes here.

#### General Advice

- Use virtual environment or conda environment to track dependencies.
- Put the hardcoded variables in config file before file is pushed for deployment.
- Set the environment variable in a script before using them.

#### Follow this steps to define path.

- Set BASE_PATH variable in config.py file. This variable should point to your project folder. Here project_name is the project folder. Put absolute path there.
- Import config.py file in your python script or notebook.
- Create configuration object (cfg) in your script and use it to construct a path.
- Checkout running examples are in index.py files and app.ipynb file.

#### These recommendations are for airflow project only.
